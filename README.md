# Custom configuration for GitLab Compose Kit

This project tracks my custom configuration for the
[GitLab Compose Kit](https://gitlab.com/gitlab-org/gitlab-compose-kit).

## Author

Tomasz Maczukin, 2021

## License

MIT

